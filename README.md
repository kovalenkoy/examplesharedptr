# ExampleSharedPtr

## Build
```
cmake -B build
cmake --build .\build\
```

## Run test on windows
```
.\build\Debug\ExampleSharedPtr.exe
```


## Run test on linux
```
cd .\build
ctest -T memcheck
```