#include "ExampleSharedPtr.h"

#include<iostream>
#include <string>

#ifdef  _WIN32

#include <windows.h>
#include <psapi.h>

uint64_t getProcessPhysicalMemory() {
	auto myHandle = GetCurrentProcess();
	PROCESS_MEMORY_COUNTERS pmc;
	BOOL result = GetProcessMemoryInfo(myHandle, &pmc, sizeof(pmc));
	
	return result == 0 ? 0 : pmc.WorkingSetSize;
}

void printLog(uint64_t start, uint64_t end, const std::string& tag)
{
	std::cout << "*********************" << std::endl;
	std::cout << tag << std::endl;
	std::cout << "*********************" << std::endl;
	std::cout << "Memory at start: " << start << " B" << std::endl;
	std::cout << "Memory at end  : " << end << " B" << std::endl;

	if (end > start)
	{
		std::cout << "Delta: " << end - start << " B" << std::endl;
	}
	else
	{
		std::cout << "Delta: zero" << std::endl;
	}
}

#endif //  WIN_32

struct Test
{
	int testData = 0;
};

int main(int argc, const char** argv)
{
#ifdef  _WIN32
	auto startMemory = getProcessPhysicalMemory();
#endif //  WIN_32

	for (size_t i = 0; i < 10000000; ++i)
	{
		ExampleSharedPtr<Test> myFirstPtr(new Test());
		(*myFirstPtr).testData = 100;
		auto mySecondPtr = myFirstPtr;
		ExampleSharedPtr<Test> thPtr;
		const ExampleSharedPtr<Test>& refPtr = myFirstPtr;
		thPtr = myFirstPtr;
		thPtr.reset(new Test());
	}

#ifdef  _WIN32
	auto endMemory = getProcessPhysicalMemory();
	printLog(startMemory, endMemory, "Memory usage");
#endif //  WIN_32

	return 0;
}