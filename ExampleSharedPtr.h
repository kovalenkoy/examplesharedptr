#pragma once

#include <atomic>
#include <assert.h>

class ExampleSharedCount
{
public:
    ExampleSharedCount() noexcept
        : m_useCount(1)
    {
    }

    ~ExampleSharedCount() = default;

    void addRefCopy() noexcept
    {
        m_useCount.fetch_add(1, std::memory_order_relaxed);
    }

    void release() noexcept
    {
        m_useCount.fetch_sub(1, std::memory_order_acq_rel);
    }

    long useCount() const noexcept
    {
        return m_useCount.load(std::memory_order_acquire);
    }

private:
    ExampleSharedCount(const ExampleSharedCount& rhs) = delete;
    ExampleSharedCount& operator=(const ExampleSharedCount& rhs) = delete;
    ExampleSharedCount(ExampleSharedCount&& rhs) = delete;
    ExampleSharedCount& operator=(ExampleSharedCount&& rhs) = delete;

    std::atomic_int_least32_t m_useCount;
};

template <typename T>
class ExampleSharedPtr
{
public:
    ExampleSharedPtr() noexcept;
    explicit ExampleSharedPtr(T* ptr) noexcept;
    ExampleSharedPtr(const ExampleSharedPtr<T>& rhs) noexcept;
    ~ExampleSharedPtr() noexcept;

    ExampleSharedPtr<T>& operator=(const ExampleSharedPtr<T>& rhs) noexcept;
    T& operator*() const noexcept;

    void reset(T* ptr) noexcept;

private:
    void reset() noexcept;
    void swap(ExampleSharedPtr<T>& rhs) noexcept;

    ExampleSharedPtr(ExampleSharedPtr<T>&& rhs) = delete;
    ExampleSharedPtr<T>& operator=(ExampleSharedPtr<T>&& rhs) = delete;

    T* m_ptr;
    ExampleSharedCount* m_counter;
};

template <typename T>
ExampleSharedPtr<T>::ExampleSharedPtr() noexcept
    : m_ptr(nullptr), m_counter(nullptr)
{
}

template <typename T>
ExampleSharedPtr<T>::ExampleSharedPtr(T* ptr) noexcept
    : m_ptr(ptr), m_counter(new ExampleSharedCount())
{
}

template <typename T>
ExampleSharedPtr<T>::ExampleSharedPtr(const ExampleSharedPtr& rhs) noexcept
    : m_ptr(rhs.m_ptr), m_counter(rhs.m_counter)
{
    if(m_counter)
    {
        m_counter->addRefCopy();
    }
}

template <typename T>
ExampleSharedPtr<T>::~ExampleSharedPtr() noexcept
{
    reset();
}

template <typename T>
ExampleSharedPtr<T>& ExampleSharedPtr<T>::operator=(const ExampleSharedPtr<T>& rhs) noexcept
{
    if(this != &rhs)
    {
        ExampleSharedPtr<T>(rhs).swap(*this);
    }
    return (*this);
}

template <typename T>
T& ExampleSharedPtr<T>::operator*() const noexcept
{
    return (*m_ptr);
}

template <typename T>
void ExampleSharedPtr<T>::reset(T* ptr) noexcept
{
    ExampleSharedPtr<T>(ptr).swap(*this);
}

template <typename T>
void ExampleSharedPtr<T>::reset() noexcept
{
    if(m_counter)
    {
        m_counter->release();

        if(m_counter->useCount() == 0)
        {
            delete m_ptr;
            delete m_counter;
        }

        m_ptr = nullptr;
        m_counter = nullptr;
    }
}

template <typename T>
void ExampleSharedPtr<T>::swap(ExampleSharedPtr<T>& rhs) noexcept
{
    std::swap(m_ptr, rhs.m_ptr);
    std::swap(m_counter, rhs.m_counter);
}